define(['views/crypto-details', 'service-get-data'], function(DetailView, ServiceData){

    function start(){
        var cryptoName = ServiceData.getCryptoName();
        ServiceData.getCryptoSelected(showCrypto, cryptoName);
    }
    
    function showCrypto(details){
        DetailView.render(details);  
    }

    return {
        start: start
    };
});