define(['views/crypto-list', 'service-get-data'], function(ListView, ServiceData){

    function start(){
        ServiceData.getCryptos(list);
    }

    function searchList(){

    }

    function list(results){
        ListView.render({cryptos: results })
        ListView.bind(searchCoin); 
        ListView.bindClick(coinDetails); 

    }

    function coinDetails(event){
        var selectedCryptoName = event.currentTarget.id;
        ServiceData.setCryptoName(selectedCryptoName);
        window.location.hash = '#details';
    }

    function searchCoin(coin){
       ServiceData.getCrypto(list,coin);
    }
    return{
        start: start
    };

});