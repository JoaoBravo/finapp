define(function(){
    function Crypto(symbol,name,rank,price_eur,id){
        this.symbol = symbol;
        this.name = name;
        this.rank = rank;
        this.price_eur = price_eur;
        this.id = id;
    }

    return Crypto;
})