define(function() {
    function Details (symbol, name, rank, price_usd, price_eur, price_btc, market_cap_usd, available_supply, total_supply){
        this.symbol = symbol;
        this.name = name;
        this.rank = rank;
        this.price_usd = price_usd;
        this.price_eur = price_eur;
        this.price_btc = price_btc;
        this.market_cap_usd = market_cap_usd;
        this.available_supply = available_supply;
        this.total_supply = total_supply;
    }

    return Details;
})

