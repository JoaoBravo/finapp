define(['models/crypto', 'models/details'], function(Crypto, Details){
    var crypto;


function fetchDataApi(path,cb){

    $.ajax({
        url: 'https://api.coinmarketcap.com/v1/' + path,
        type: 'GET',
        dataType: 'json',
        success: function(results){
            cb(null, results);
        },
        error: processError
    })
}

function setCryptoName(name){
    crypto = name;
}

function getCryptoName(){
    return crypto;
}

function getCryptos(cb){
    var path = 'ticker/?convert=EUR&start='+ 0 +'&limit=' + 100;

    fetchDataApi(path, function(error, results){
        if(error){
            console.log('error');
        }

        var result= results.map(function(crypto){
            var newCrypto = new Crypto(crypto.symbol, crypto.name, crypto.rank, crypto.price_eur, crypto.id);
            return newCrypto;
        })

        cb(result);

    })

}

function getCrypto(cb, coin){
    var path = 'ticker/'+coin+'/?convert=EUR';
    console.log(coin);

    fetchDataApi(path, function(error, results){
        if(error){
            alert('error searching the crypto!')
        }

        var result= results.map(function(crypto){
            var newCrypto = new Crypto(crypto.symbol, crypto.name, crypto.rank, crypto.price_eur, crypto.id);
            return newCrypto;
        })
        console.log(result);
        cb(result);

    })


}

function getCryptoSelected(cb, selectedCrypto){
    var path = 'ticker/' + selectedCrypto + '/?convert=EUR';

    fetchDataApi(path, function(error, results){
        if(error){
            alert('error checking the crypto!')
        }

        var result = results.map(function(crypto){
            var DetailCrypto = new Details(crypto.symbol,crypto.name,crypto.rank,crypto.price_usd,crypto.price_eur,crypto.price_btc,crypto.market_cap_usd,crypto.available_supply,crypto.total_supply);
            return DetailCrypto;
        })
        console.log(result);
        cb(result[0]);
    })
}

function processError(){
    alert('Error! Try again!');
}

return{
    getCryptos: getCryptos,
    getCryptoName: getCryptoName,
    setCryptoName: setCryptoName,
    getCrypto: getCrypto,
    getCryptoSelected: getCryptoSelected
};

})
