define(function(){

    function render(params){
        var appDiv = document.getElementById('app');
        console.log(params);
        title = writeTitle(params.name,params.rank);
        prices = writePrices(params.price_usd,params.price_eur,params.price_btc);
        supply = writeSupply(params.market_cap_usd,params.available_supply,params.total_supply);

        appDiv.innerHTML = title + prices + supply;

    }

    function writeTitle(name, rank){
        return '<h1>' + name + '-' + rank + '</h1>';
    }

    function writePrices(usd, euro, btc){
        return '<h2>Prices</h2> <ul style="list-style:none" > <li> $ ' + usd + '</li> <li> € ' + euro + '</li> <li> btc - ' + btc + '</li></ul>';
    }

    function writeSupply(market_cap_usd, available_supply, total_supply){
        return '<h3>Supply</h3>' +
        '<ul style="list-style:none">' +
        '<li> Market Cap (USD): ' + market_cap_usd + '</li>'+
        '<li> Available Supply: ' + available_supply + '</li>'+
        '<li> Total Supply:' + total_supply+ '</li>'+
        '</ul>'
    };

    return{
        render: render
    }
});