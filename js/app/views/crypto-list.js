define(['controllers/crypto-list-controller'],function(ControllerList){

    function render(parameters){

        if(parameters){
            var appDiv = document.getElementById('app');
            appDiv.innerHTML=drawSearch();
            appDiv.innerHTML+=drawList(parameters);
        }
        
    }
    function drawSearch(){
        return '<button id="search">Search</button><input type="text" id="input"/>'
    }

    function drawList(parameters){
        var cryptos = parameters.cryptos;
        var htmlWrite = tableHeader();
        var appDiv = document.getElementById('app');

        for (var i = 0, len = cryptos.length; i < len; i++){
            htmlWrite += row(cryptos[i]);
        }
    
        return '<table align="center">' + htmlWrite + '</table>'
    }


    function tableHeader(){
        return '<th><tr>'+'<td>Rank</td><td>Name</td><td>Symbol</td><td>Price €</td>'+'</tr></th>'
    }

    function row (cryptos){
        rows =[];

        rows.push(
                '<tr class="crypto-id" id="' + cryptos.id + '">' +
                '<td>' + cryptos.rank + '</td>' +
                '<td><a>' + cryptos.name + '</a></td>'+
                '<td>' + cryptos.symbol + '</td>' +
                '<td>' + cryptos.price_eur + '</td>' +
                '</tr>'
        )
        return rows;
    }

    function bindClick(handler){
        console.log('cliked');
        $('.crypto-id').on('click',handler);
    }

    function bind(handler){
        $("#search").click(function(){
            var inputSearch = $("#input").val();
            handler(inputSearch);
        });
    }

    return{
        render:render,
        bind: bind,
        bindClick: bindClick
    };
});